package br.com.southsystem.accountlimit.resource;

import br.com.southsystem.accountlimit.model.request.AccountLimitRequest;
import br.com.southsystem.accountlimit.model.response.AccountLimitCreateResponse;
import br.com.southsystem.accountlimit.service.AccountLimitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account-limit")
public class AccountLimitResource {

    private final AccountLimitService service;

    public AccountLimitResource(AccountLimitService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<AccountLimitCreateResponse> create
            (@RequestBody AccountLimitRequest request) {
        return ResponseEntity.ok(service.create(request));
    }
}
