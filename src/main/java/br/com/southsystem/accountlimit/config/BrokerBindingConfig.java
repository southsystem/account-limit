package br.com.southsystem.accountlimit.config;

import br.com.southsystem.accountlimit.binder.BrokerInput;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({BrokerInput.class})
public class BrokerBindingConfig {
}
