package br.com.southsystem.accountlimit.model.response;

import br.com.southsystem.accountlimit.model.entity.Limit;

import java.math.BigDecimal;

public class AccountLimitCreateResponse {

    private String idLimit;
    private BigDecimal value;

    public AccountLimitCreateResponse(Limit limit) {
        idLimit = limit.getIdLimit();
        value = limit.getValue();
    }

    public String getIdLimit() {
        return idLimit;
    }

    public void setIdLimit(String idLimit) {
        this.idLimit = idLimit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
