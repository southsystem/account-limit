package br.com.southsystem.accountlimit.service;

import br.com.southsystem.accountlimit.binder.BrokerInput;
import br.com.southsystem.accountlimit.model.entity.Limit;
import br.com.southsystem.accountlimit.model.request.AccountLimitRequest;
import br.com.southsystem.accountlimit.model.response.AccountLimitCreateResponse;
import br.com.southsystem.accountlimit.repository.LimitRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountLimitService {

    private final LimitRepository repository;
    @Value("${limit.default.value}")
    private BigDecimal limitValue;

    public AccountLimitService(LimitRepository repository) {
        this.repository = repository;
    }

    public AccountLimitCreateResponse create(AccountLimitRequest request) {
        Limit limit = new Limit(request);
        limit.setValue(limitValue);
        return new AccountLimitCreateResponse(repository.save(limit));
    }

    @StreamListener(BrokerInput.accountCreated)
    public void accountCreated(AccountLimitRequest request) {
        create(request);
    }
}
